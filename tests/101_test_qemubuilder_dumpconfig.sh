#!/bin/bash
# check qemubuilder commnad-line option parsing.
#
# Apparently, this test requires config files to exist in order not to
# die of error, so we need to have qemubuilder installed.
set -ex

[ "$(./qemubuilder --dumpconfig --inputfile one --inputfile two | grep inputfile)" = \
"  inputfile[0]: one
  inputfile[1]: two" ]
